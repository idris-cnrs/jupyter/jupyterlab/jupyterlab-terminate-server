/* -----------------------------------------------------------------------------
| Copyright (c) Jupyter Development Team.
| Distributed under the terms of the Modified BSD License.
|----------------------------------------------------------------------------*/
/**
 * @packageDocumentation
 * @module @jupyterlab/terminate-server
 */

import {
  ConnectionLost,
  IConnectionLost,
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';
import { Dialog, showDialog } from '@jupyterlab/apputils';
import { ServerConnection, ServiceManager } from '@jupyterlab/services';
import { ITranslator } from '@jupyterlab/translation';

/**
 * The command IDs used by the plugin.
 */
export namespace CommandIDs {
  export const terminate: string = 'hub:terminate';
}

/**
 * Call an async function with a maximum time limit (in milliseconds) for the timeout
 * @param {Promise<any>} asyncPromise An asynchronous promise to resolve
 * @param {number} timeLimit Time limit to attempt function in milliseconds
 * @returns {Promise<any> | undefined} Resolved promise for async function call, or an error if time limit reached
 */
 const asyncCallWithTimeout = async (
  asyncPromise: Promise<any>,
  timeLimit: number
  ) => {
  let timeoutHandle: any;

  const timeoutPromise = new Promise((_resolve, reject) => {
      timeoutHandle = setTimeout(
          () => reject(new Error('Async call timeout limit reached')),
          timeLimit
      );
  });

  return Promise.race([asyncPromise, timeoutPromise]).then(result => {
      clearTimeout(timeoutHandle);
      return result;
  })
}

/**
 * The default JupyterLab connection lost provider. This may be overridden
 * to provide custom behavior when a connection to the server is lost.
 *
 * If the application is being deployed within a JupyterHub context,
 * this will provide a dialog that prompts the user to restart the server.
 * Otherwise, it shows an error dialog.
 */
const terminateConnection: JupyterFrontEndPlugin<IConnectionLost> = {
  id: 'jupyterlab-terminate-server',
  requires: [JupyterFrontEnd.IPaths, ITranslator],
  activate: (
    app: JupyterFrontEnd,
    paths: JupyterFrontEnd.IPaths,
    translator: ITranslator
  ): IConnectionLost => {
    const trans = translator.load('jupyterlab');
    const hubPrefix = paths.urls.hubPrefix || '';
    const baseUrl = paths.urls.base;

    const { commands } = app;

    commands.addCommand(CommandIDs.terminate, {
      label: trans.__('Terminate Server'),
      caption: trans.__('Request to terminate this server and close tab'),
      isVisible: () => false,
      execute: () => {
        window.close();
      }
    });

    // Return the default error message if not running on JupyterHub.
    if (!hubPrefix) {
      return ConnectionLost;
    }

    // If we are running on JupyterHub, return a dialog
    // that prompts the user to restart their server.
    let showingError = false;
    const onConnectionLost: IConnectionLost = async (
      manager: ServiceManager.IManager,
      err: ServerConnection.NetworkError
    ): Promise<void> => {
      if (showingError) {
        return;
      }

      showingError = true;

      const dialog = showDialog({
        title: trans.__('Server unavailable or unreachable'),
        body: trans.__(
          'Your server at %1 is not running.\nWould you like to restart it?',
          baseUrl
        ),
        buttons: [
          Dialog.okButton({ label: trans.__('Restart') }),
          Dialog.cancelButton({ label: trans.__('Dismiss') })
        ]
      });

      // Wait for the user to provide an input to dialog. If there is no
      // interaction from the user for 1 hour, we close the server window
      // and internal culling will take care of culling server
      try {
        const result = await asyncCallWithTimeout(dialog, 3600000);
        showingError = false;

        if (result.button.accept) {
          await app.commands.execute('hub:restart');
        }
      }
      catch (err) {
          console.error(
            'No user input detected for more than 1 hour. Terminating server...'
          );
          await app.commands.execute(CommandIDs.terminate);
      }
    };
    return onConnectionLost;
  },
  autoStart: true,
  provides: IConnectionLost
};

export default [
  terminateConnection
] as JupyterFrontEndPlugin<any>[];
